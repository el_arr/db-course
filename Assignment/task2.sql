﻿CREATE TABLE main (
  clusterID    VARCHAR(30) NOT NULL,
  nodeID       VARCHAR(30) NOT NULL,
  manufacturer VARCHAR(30) NOT NULL,
  PRIMARY KEY (clusterID, nodeID, manufacturer)
);

CREATE TABLE state_prev (
  clusterID    VARCHAR(30) NOT NULL,
  nodeID       VARCHAR(30) NOT NULL,
  manufacturer VARCHAR(30) NOT NULL,
  PRIMARY KEY (clusterID, nodeID, manufacturer)
);

CREATE TABLE state_log (
  clusterID    VARCHAR(30) NOT NULL,
  nodeID       VARCHAR(30) NOT NULL,
  manufacturer VARCHAR(30) NOT NULL,
  date         DATE DEFAULT current_date,
  action       VARCHAR(30) NOT NULL,
  FOREIGN KEY (clusterID, nodeID, manufacturer) REFERENCES main (clusterID, nodeID, manufacturer)
);

INSERT INTO main VALUES
  ('cluster1', 'node2', 'M1'),
  ('cluster2', 'node1', 'M2');

INSERT INTO state_prev VALUES
  ('cluster1', 'node1', 'M1'),
  ('cluster1', 'node2', 'M1');

-- After add
INSERT INTO state_log (clusterID, nodeID, manufacturer, date, action)
  (
    SELECT
      main.clusterID,
      main.nodeID,
      main.manufacturer,
      current_date,
      'add'
    FROM main
      LEFT OUTER JOIN state_prev ON main.nodeID = state_prev.nodeID
                                    AND main.clusterID = state_prev.clusterID
                                    AND main.manufacturer = state_prev.manufacturer
  );

-- After delete
WITH previous_state AS (
    SELECT *
    FROM main
      RIGHT OUTER JOIN state_prev ON main.nodeID = state_prev.nodeID
                                     AND main.clusterID = state_prev.clusterID
                                     AND main.manufacturer = state_prev.manufacturer
)
INSERT INTO state_log (clusterID, nodeID, manufacturer, date, action)
  (
    SELECT
      main.clusterID,
      main.nodeID,
      main.manufacturer,
      current_date,
      'remove'
    FROM main
    WHERE main.clusterID = previous_state.clusterID
          AND main.nodeID = previous_state.nodeID
          AND main.manufacturer = previous_state.manufacturer
  );

-- After add and delete
DELETE FROM state_prev;
INSERT INTO state_prev (
  SELECT *
  FROM main
);


SELECT (clusterID, nodeID, manufacturer)
FROM state_log
WHERE action = 'add' AND date >= current_date - 10;


SELECT
  clusterID,
  nodeID,
  manufacturer
FROM state_log
WHERE action = 'remove'
ORDER BY date ASC
LIMIT 10;