CREATE TABLE user_profile (
  username   VARCHAR(30)  NOT NULL UNIQUE,
  login      VARCHAR(20)  NOT NULL UNIQUE,
  password   VARCHAR(100) NOT NULL,
  email      VARCHAR(30)  NOT NULL UNIQUE,
  city       VARCHAR(30),
  year_birth INT,
  isAdmin    BOOLEAN      DEFAULT FALSE,
  token      VARCHAR(100) DEFAULT '',
  id         SERIAL       NOT NULL PRIMARY KEY,
  CHECK (year_birth > 1900)
);

CREATE TABLE user_admin (
  id      SERIAL NOT NULL PRIMARY KEY,
  user_id INT    NOT NULL REFERENCES user_profile (id)
);

CREATE TABLE topic (
  name VARCHAR(50) NOT NULL,
  id   SERIAL      NOT NULL PRIMARY KEY
);

CREATE TABLE book (
  name        VARCHAR(50) NOT NULL UNIQUE,
  description TEXT,
  cover_path  VARCHAR(50) NOT NULL,
  topic_id    INT         NOT NULL REFERENCES topic (id),
  year_pub    INT CHECK (year_pub > 0),
  pages       INT CHECK (pages > 0),
  intro_path  VARCHAR(50),
  language    VARCHAR(20) DEFAULT 'русский',
  is_Popular  BOOLEAN     DEFAULT FALSE,
  is_New      BOOLEAN     DEFAULT FALSE,
  id          SERIAL      NOT NULL PRIMARY KEY
);

CREATE TABLE author (
  name      VARCHAR(50) NOT NULL UNIQUE,
  biography TEXT,
  img_path  VARCHAR(50) NOT NULL DEFAULT '',
  id        SERIAL      NOT NULL PRIMARY KEY
);

CREATE TABLE review (
  user_id       INT    NOT NULL REFERENCES user_profile (id),
  book_id       INT    NOT NULL REFERENCES book (id),
  text          TEXT   NOT NULL,
  mark          INT    NOT NULL,
  title         VARCHAR(50)                 DEFAULT '',
  like_quantity INT    NOT NULL             DEFAULT 0,
  data_pub      DATE   NOT NULL             DEFAULT CURRENT_DATE,
  time_pub      TIME   NOT NULL             DEFAULT CURRENT_TIME,
  id            SERIAL NOT NULL PRIMARY KEY,
  CHECK (mark >= 0),
  CHECK (mark <= 10)
);

CREATE TABLE genre (
  name VARCHAR(50) NOT NULL UNIQUE,
  id   SERIAL      NOT NULL PRIMARY KEY
);

CREATE TABLE shop (
  name VARCHAR(30) NOT NULL,
  id   SERIAL      NOT NULL PRIMARY KEY
);

CREATE TABLE book_author (
  book_id   INT NOT NULL REFERENCES book (id),
  author_id INT NOT NULL REFERENCES author (id),
  CONSTRAINT pk_book_author_id PRIMARY KEY (book_id, author_id)
);

CREATE TABLE book_genre (
  book_id  INT NOT NULL REFERENCES book (id), ,
  genre_id INT NOT NULL REFERENCES genre (id),
  CONSTRAINT pk_book_genre_id PRIMARY KEY (book_id, genre_id)
);

CREATE TABLE topic_record (
  user_id  INT    NOT NULL REFERENCES user_profile (id),
  topic_id INT    NOT NULL REFERENCES topic (id),
  text     TEXT   NOT NULL,
  data_pub DATE DEFAULT CURRENT_DATE,
  time_pub TIME DEFAULT CURRENT_TIME,
  id       SERIAL NOT NULL PRIMARY KEY
);

CREATE TABLE book_shop (
  shop_id INT          NOT NULL REFERENCES shop (id),
  book_id INT          NOT NULL REFERENCES book (id),
  link    VARCHAR(100) NOT NULL,
  id      SERIAL       NOT NULL PRIMARY KEY
);