CREATE OR REPLACE FUNCTION user_log()
  RETURNS TRIGGER AS $$
BEGIN
  IF tg_op = 'INSERT'
  THEN
    INSERT INTO _user_log VALUES ('User added: ' || NEW.login);
    RETURN NEW;
  ELSEIF tg_op = 'UPDATE'
    THEN
      INSERT INTO _user_log VALUES ('User updated: ' || NEW.login);
      RETURN NEW;
  ELSEIF tg_op = 'DELETE'
    THEN
      INSERT INTO _user_log VALUES ('User removed: ' || OLD.login);
      RETURN OLD;
  END IF;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER user_history_log
BEFORE INSERT OR UPDATE OR DELETE ON user_profile
FOR EACH ROW EXECUTE PROCEDURE user_log();


CREATE OR REPLACE FUNCTION book_log()
  RETURNS TRIGGER AS $$
BEGIN
  IF tg_op = 'INSERT'
  THEN
    INSERT INTO _book_log VALUES ('Book added: ' || NEW.name);
    RETURN NEW;
  ELSEIF tg_op = 'UPDATE'
    THEN
      INSERT INTO _book_log VALUES ('Book updated: ' || NEW.name);
      RETURN NEW;
  ELSEIF tg_op = 'DELETE'
    THEN
      INSERT INTO _book_log VALUES ('Book removed: ' || OLD.name);
      RETURN OLD;
  END IF;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER book_history_log
BEFORE INSERT OR UPDATE OR DELETE ON book
FOR EACH ROW EXECUTE PROCEDURE book_log();


CREATE OR REPLACE FUNCTION author_log()
  RETURNS TRIGGER AS $$
BEGIN
  IF tg_op = 'INSERT'
  THEN
    INSERT INTO _author_log VALUES ('Author added: ' || NEW.name);
    RETURN NEW;
  ELSEIF tg_op = 'UPDATE'
    THEN
      INSERT INTO _author_log VALUES ('Author updated: ' || NEW.name);
      RETURN NEW;
  ELSEIF tg_op = 'DELETE'
    THEN
      INSERT INTO _author_log VALUES ('Author removed: ' || OLD.name);
      RETURN OLD;
  END IF;
END;
$$ LANGUAGE plpgsql;
CREATE TRIGGER author_history_log
BEFORE INSERT OR UPDATE OR DELETE ON author
FOR EACH ROW EXECUTE PROCEDURE author_log();